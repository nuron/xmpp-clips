## Requesting a new clip

#### General information
- **This clip should advertise for:** Server/Client/Plattform/..
- **This icon should be used:** url
- **The icon is published under this license:** license

#### Content
**number of "slides" needed:** 5
**number of seconds a "slide" should be visible:** 3 
**The following content should be on the "slides":** (one slide - one row)
 - icon
 - first content
 - ...
 - ....
 - last content

**Text color:** #000000
**Background color:** #ffffff

**This music/sound should be used:** url
**with this timestamp** 01:23-01:34
**This music/sound is published under this license:** license
